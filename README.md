Использованные технологии при создании проекта:
    - HTML;
    - CSS;
    - JavaScript;
    - Gulp.

Состав участников:
    - Борзенко Константин;
    - Вердиев Виталий.

Задачи:
    - задание 1 выполнял Константин;
    - задание 2 было закреплено за Виталием.