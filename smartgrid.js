module.exports = {
    outputStyle: "scss",
    columns: 12,
    offset: "15px",
    // mobileFirst: true,
    container: {
        maxWidth: "1280px",
        fields: "30px"
    },
    breakPoints: {
        lg: {
            width: "1200px"
        },
        md: {
            width: "992px",
            fields: "15px"
        },
        sm: {
            width: "720px"
        },
        xs: {
            width: "576px"
        }
    },
    // mixinNames: {
    //     container: "container"
    // }
};